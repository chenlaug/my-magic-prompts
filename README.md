# My Magic Prompt

## Introduction

Bienvenue sur My Magic Prompt, un environnement interactif en ligne de commande conçu pour offrir des fonctionnalités supplémentaires au shell traditionnel. Ce projet est hébergé dans le répertoire `~/my-magic-prompt/main.sh`.

Pour accéder au prompt, vous devrez fournir un login et un mot de passe spécifiques.

## Commandes disponibles

### Générales

- **help**: Affiche les commandes disponibles.
- **quit**: Quitte le prompt.

### Gestion des fichiers et dossiers

- **ls**: Liste les fichiers et les dossiers, y compris les fichiers cachés.
- **rm**: Supprime un fichier.
- **rmd** ou **rmdir**: Supprime un dossier.
- **cd**: Change le répertoire courant.
- **pwd**: Affiche le répertoire actuel.
- **open**: Ouvre un fichier dans l'éditeur VIM (crée le fichier s'il n'existe pas).

### Informations

- **about**: Affiche une description du programme.
- **version** ou **--v** ou **vers**: Affiche la version actuelle du prompt.
- **profil**: Affiche des informations sur vous-même, notamment le prénom, le nom, l'âge et l'email.
- **hour**: Affiche l'heure actuelle.

### Utilisateur

- **age**: Demande votre âge et indique si vous êtes majeur ou mineur.
- **passw**: Permet de changer le mot de passe après confirmation.

### Erreurs

- **\***: Indique une commande inconnue.

## Comment utiliser

1. Exécutez `main.sh` pour lancer le prompt.
2. Utilisez les commandes ci-dessus pour interagir avec le prompt.

## Auteur

Laughan chenevot

## Version

1.0.0

---

Pour toute autre question ou suggestion, n'hésitez pas à ouvrir une issue ou à contribuer au projet.
