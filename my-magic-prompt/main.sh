#!/bin/bash

# Définir le répertoire et le fichier de configuration
config_dir="/home/laughan/prompt/mymagicprompt/my-magic-prompt"
config_file="$config_dir/.auth"

# Créer le répertoire s'il n'existe pas
if [[ ! -d $config_dir ]]; then
    mkdir -p $config_dir
fi

# Si le fichier de configuration n'existe pas, demander à l'utilisateur de créer un utilisateur et un mot de passe
if [[ ! -e $config_file ]]; then
  read -p "Créer un mon d'utilisateur : " username
  read -s -p "Créer un mot de pass : " password
  echo ""
  
  salt=$(openssl rand -base64 6)
  hashed_password=$(echo "$password$salt" | sha256sum | cut -d' ' -f1)
  
  echo "username=$username" > $config_file
  echo "hashed_password=$hashed_password" >> $config_file
  echo "salt=$salt" >> $config_file
fi

# Charger les valeurs depuis le fichier de configuration
source $config_file

# Authentification de l'utilisateur
read -p "Username: " entered_username
read -s -p "Password: " entered_password
echo ""

entered_hashed_password=$(echo "$entered_password$salt" | sha256sum | cut -d' ' -f1)

if [[ "$entered_username" != "$username" || "$entered_hashed_password" != "$hashed_password" ]]; then
  echo "Connexion incorrecte !"
  exit 1
fi

rmdirwtf() {
    # Demander et vérifier le mot de passe
    read -s -p "Entrez le mot de passe pour la suppression: " entered_password
    echo ""
    entered_hashed_password=$(echo "$entered_password$salt" | sha256sum | cut -d' ' -f1)

    if [[ "$entered_hashed_password" != "$hashed_password" ]]; then
        echo "Mot de passe incorrect !"
        return
    fi

    # Demander les dossiers et fichiers à supprimer
    read -p "Entrez les noms des fichiers et dossiers à supprimer (séparés par des espaces) : " -a items
    
    # Supprimer chaque fichier/dossier entré par l'utilisateur
    for item in "${items[@]}"; do
        if [ -e "$item" ]; then
            rm -r "$item"
            echo "L'élément $item a été supprimé."
        else
            echo "$item n'est pas un fichier ou dossier valide."
        fi
    done
}

change_directory() {
    read -p "Entrez le nom du répertoire: " dir
    cd "$dir" 2> /dev/null || echo "Répertoire non valide : $dir"
}

open_file() {
    read -p "Entrez le nom du fichier à ouvrir/à créer avec vim: " filename
    vim "$filename"
}

download_webpage() {
    read -p "Entrez l'URL de la page web que vous souhaitez télécharger: " url
    read -p "Entrez le nom du fichier dans lequel vous souhaitez sauvegarder le code source HTML: " filename
    curl -s "$url" -o "$filename" 
    if [ $? -eq 0 ]; then
        echo "La page web a été téléchargée avec succès dans $filename."
    else
        echo "Erreur lors du téléchargement de la page web."
    fi
}

change_password(){
  echo "Changement du mot de passe..."
  
  attempt=1
  while [ $attempt -le 3 ]; do
    read -s -p "Entrez le mot de passe actuel: " current_password
    echo ""
    current_hashed_password=$(echo "$current_password$salt" | sha256sum | cut -d' ' -f1)
    
    if [[ "$current_hashed_password" == "$hashed_password" ]]; then
      break
    else
      echo "Le mot de passe actuel est incorrect !"
      if [ $attempt -eq 3 ]; then
        echo "Vous avez échoué 3 fois. Retour au prompt."
        return
      else
        echo "Essai $(($attempt + 1))"
      fi
    fi
    attempt=$(($attempt + 1))
  done
  
  # Demander le nouveau mot de passe
  read -s -p "Entrez le nouveau mot de passe: " new_password
  echo ""
  read -s -p "Confirmez le nouveau mot de passe: " confirm_password
  echo ""
  
  # Vérification de la correspondance des mots de passe
  if [[ "$new_password" != "$confirm_password" ]]; then
    echo "Les mots de passe ne correspondent pas !"
    return
  fi
  
  # Mettre à jour le fichier de configuration avec le nouveau mot de passe
  new_salt=$(openssl rand -base64 6)
  new_hashed_password=$(echo "$new_password$new_salt" | sha256sum | cut -d' ' -f1)
  
  echo "Mise à jour du fichier de configuration..."
  echo "username=$username" > $config_file
  echo "hashed_password=$new_hashed_password" >> $config_file
  echo "salt=$new_salt" >> $config_file
  
  echo "Le mot de passe a été modifié avec succès !"
}

ask_age(){
  read -p "Entrez votre âge : " age
  if ! [[ "$age" =~ ^[0-9]+$ ]]; then
    echo "Veuillez entrer un nombre valide pour l'âge."
  elif [ "$age" -ge 18 ]; then
    echo "Vous êtes majeur."
  else
    echo "Vous êtes mineur."
  fi
}

ask_profil(){
  read -p "Entrez votre prénom : " first_name
  read -p "Entrez votre nom de famille : " last_name
  read -p "Entrez votre âge : " age
  read -p "Entrez votre e-mail : " email
  
  # Vérification de l'âge
  if ! [[ "$age" =~ ^[0-9]+$ ]]; then
    echo "Veuillez entrer un nombre valide pour l'âge."
    return
  fi
  
  # Affichage du profil
  echo "-------------------------------------"
  echo "Profil:"
  echo "Prénom: $first_name"
  echo "Nom de famille: $last_name"
  echo "Âge: $age"
  echo "E-mail: $email"
  echo "-------------------------------------"
}

show_help(){
echo "Commande disponible :"
echo "help: Affiche les commandes disponibles."
echo "quit: Quitte le prompt."
echo "ls: Liste les fichiers et les dossiers, y compris les fichiers cachés."
echo "rm: Supprime un fichier."
echo "rmd ou rmdir: Supprime un dossier."
echo "cd: Change le répertoire courant."
echo "pwd: Affiche le répertoire actuel."
echo "open: Ouvre un fichier dans l'éditeur VIM (crée le fichier s'il n'existe pas)."
echo "about: Affiche une description du programme."
echo "version ou --v ou vers: Affiche la version actuelle du prompt."
echo "profil: Affiche des informations sur vous-même, notamment le prénom, le nom, l'âge et l'email."
echo "hour: Affiche l'heure actuelle."
echo "age: Demande votre âge et indique si vous êtes majeur ou mineur."
echo "passw: Permet de changer le mot de passe après confirmation."
echo "*: Indique une commande inconnue."
}

show_about(){
  echo "-------------------------------------"
  echo "Bienvenue dans le Script Magique!"
  echo "-------------------------------------"
  echo "Ce script n’est pas simplement un script, c’est une baguette magique qui transforme vos commandes en actions!"
  echo "Il ne vous permet pas seulement de naviguer entre les répertoires, il vous fait voyager à travers un univers de fichiers!"
  echo "Il ne supprime pas seulement vos fichiers et dossiers, il les fait disparaître dans les abîmes de l’informatique!"
  echo "C'est le lapin sortant du chapeau pour vous montrer les secrets des répertoires cachés et les mystères de vos fichiers!"
  echo "-------------------------------------"
  echo "Que ce soit pour effacer, explorer, naviguer ou dévoiler, ce script est votre compagnon magique dans le monde enchanté du terminal!"
  echo "Alors préparez-vous, saisissez votre chapeau de sorcier et laissez la magie opérer!"
  echo "-------------------------------------"
}

show_version(){
echo "Version du prompt : 1.0.0"
}

show_version(){
echo "Version du prompt : 1.0.0"
}

execute_command(){
  local cmd="$1"
  case $cmd in
    help)
      show_help
      ;;
	  ls)
	  ls -la
	  ;;
	rm)
	read -p "Entre le nom du fichier a supprimer : " file
	if [ -f "$file" ]; then
	rm "$file" && echo "Le fichier $file a été supprimer avec succè."
	else 
	echo "Le fichier $file n'est pas un fichier valide."
	fi
	;;
	rmd|rmdir)
	read -p "Entre le nom du dossier a supprimer : " dir
	if [ -d "$dir" ]; then
	rm -r "$dir" && echo "Le dossier $dir a été supprimer avec succè."
	else 
	echo "Le dossier $dir n'est pas un dossier valide"
	fi
	;;
	about)
	show_about
	;;
	version|--v|vers)
	show_version
	;;
	age)
	ask_age
	;;
	quit)
	echo "Au revoir !"
	return 1
	;;
	profil)
	ask_profil
	;;
	passw)
	change_password
	;;
	pwd)
	pwd
	;;
	hour)
	echo "Heure actuelle : $(date +%T)"
	;;
	httpget)
	download_webpage
	;;
	open)
	open_file
	;;
	cd)
	change_directory
	;;
	rmdirwtf)
	rmdirwtf
	;;
	*)
	echo "Commande inconnue $cmd"
	;;
  esac
  return 0
}

while true; do
  read -p "Votre commande : " cmd
  execute_command "$cmd" || break
done
